exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
      // 'browserName': 'internet explorer', - special installation needed
      // 'version':'10',
      'browserName': 'chrome',
      //'browserName': 'firefox'
     },
    baseUrl:'http://127.0.0.1:3000',
    suites : {
      add: 'e2e/grocery-spec.add.js',
      search: 'e2e/grocery-spec.search.js'
    },
    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
      showColors: true,
      defaultTimeoutInterval: 30000
    }
  };