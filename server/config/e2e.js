
'use strict';

module.exports = {
    db: {
        host: 'localhost',
        username: 'root',
        password: 'password1234',
        database: 'grocerydb',
        dialet: 'sqlite',
        sync: false,
        storage: '../sqlite/grocerydb.db'
    }
};