


describe('grocery app listing #2', function() {
    it('add new product', function() {
      browser.get('http://localhost:3000/#!/add');
      element(by.model('ctrl.product.name')).sendKeys('Milo');
      element(by.model('ctrl.product.brand')).sendKeys('Nestle');
      var timestamp = new Date();
      element(by.id('upc12')).sendKeys(timestamp.toString("DDMMYYYYhmmss"));
      element(by.id('addSaveBtn')).click();
      browser.driver.sleep(1000);
      browser.waitForAngular();
      var productName = element(by.cssContainingText('.growl-message', 'Product saved. Milo'));  
      console.log(productName);
    });
  
});





function UiSelect(elem) {
    var self = this;

    self._input = elem;
    self._selectInput = self._input.element(by.css('.ui-select-search'));
    self._choices = self._input.all(by.css('.ui-select-choices .ui-select-choices-row-inner'));

    self.sendKeys = function(val) {
        self._input.click();
        self._selectInput.clear();
        return self._selectInput.sendKeys(val);
    };

    self.pickChoice = function(index){
        browser.waitForAngular();
        expect(self._choices.count()).not.toBeLessThan(index + 1);
        return self._choices.get(index).click();
    };
};