describe('grocery app listing #1', function() {
    
        it('search by brand', function() {
            browser.get('http://localhost:3000/#!/list');
            var input = new UiSelect(element(by.model('ctrl.searchType.selectedType')));
            input.sendKeys('brand'); 
            input.pickChoice(0);    
            element(by.model('ctrl.keyword')).sendKeys('Nestle');
            element(by.id('searchBtn')).click();
            var productlist = element.all(by.repeater('product in ctrl.products'));
            expect(productlist.count()).toBeGreaterThan(-1);
          });
    });



function UiSelect(elem) {
    var self = this;

    self._input = elem;
    self._selectInput = self._input.element(by.css('.ui-select-search'));
    self._choices = self._input.all(by.css('.ui-select-choices .ui-select-choices-row-inner'));

    self.sendKeys = function(val) {
        self._input.click();
        self._selectInput.clear();
        return self._selectInput.sendKeys(val);
    };

    self.pickChoice = function(index){
        browser.waitForAngular();
        expect(self._choices.count()).not.toBeLessThan(index + 1);
        return self._choices.get(index).click();
    };
};